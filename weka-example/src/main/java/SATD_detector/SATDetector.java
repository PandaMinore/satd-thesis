package main.java.SATD_detector;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import weka.attributeSelection.InfoGainAttributeEval;
import weka.attributeSelection.Ranker;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.bayes.NaiveBayesMultinomial;
import weka.classifiers.meta.FilteredClassifier;
import weka.classifiers.rules.ZeroR;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ArffLoader;
import weka.core.stemmers.SnowballStemmer;
import weka.filters.Filter;
import weka.filters.MultiFilter;
import weka.filters.supervised.attribute.AttributeSelection;
import weka.filters.unsupervised.attribute.StringToWordVector;

public class SATDetector {

	double ratio = 0.1;
	FilteredClassifier trainedClassifier;
	/*
	 * This class contains the configuration of the pre-processing and classification for the SATD_detector tool. 
	 * This is what you should use as baseline to your approach. Feel free to move this code or integrate to your other classes as it better suits your implementation.
	 * Keep in mind that this evaluation should be run for each raw training/test partition you have (either with train/test split or 
	 * with cross-validation, but the important thing is that the partitions should be the same). 
	 * By raw I mean considering the original instances without any pre-processing.
	 * 
	 * 
	 * Originally, in the SATD_detector one model was created for each system and then sequentially used over the new instance to classify.
	 * Here, for now, you are training considering all systems together, and thus only one model will be trained.
	 * Moreover, the tool was meant to classify into is or not satd (as we talked to try).
	 * 
	 * So, the alternatives are:
	 * 
	 * 1. Compare their pre-processing with yours and train and test as you were doing using the train method in this class and the evaluating as you have already done.
	 * 
	 * 2. Classify in the binary setting (check the names of the classes in RulesZ). To do this you can use their pre-processing and rules. 
	 * The rules that they used caused that evaluation could not be performed using the weka Evaluation class, so to make experiments easy,
	 * I implemented a simple RulesZ classifier that can predict classes according to both the rules and the classifier that the authors defined. Then, you can simply use the Evaluation class as before.
	 */
	
	//TODO: This is the method for the training in the multiclass scenario. Then, you need a method for performing the evaluation over the test set
	public FilteredClassifier train(Instances trainSet) throws Exception {

		StringToWordVector stw = new StringToWordVector(100000);
		stw.setOutputWordCounts(true);
		stw.setIDFTransform(true);
		stw.setTFTransform(true);
		SnowballStemmer stemmer = new SnowballStemmer();
		stw.setStemmer(stemmer);
		stw.setStopwordsHandler(new SATDStopwordsHandler());
		stw.setInputFormat(trainSet);
		//		trainSet = Filter.useFilter(trainSet, stw);

		// attribute selection for training data
		AttributeSelection attSelection = new AttributeSelection();
		Ranker ranker = new Ranker();

		ranker.setNumToSelect(500);
		InfoGainAttributeEval ifg = new InfoGainAttributeEval();
		attSelection.setEvaluator(ifg);
		attSelection.setSearch(ranker);
		
//		attSelection.setInputFormat(trainSet);
		//		trainSet = Filter.useFilter(trainSet, attSelection);

		MultiFilter mf = new MultiFilter();
		mf.setFilters(new Filter[] {stw,attSelection});
		
		trainedClassifier = new FilteredClassifier();

		trainedClassifier.setFilter(mf);
		trainedClassifier.setClassifier(new NaiveBayesMultinomial());
		
		trainedClassifier.buildClassifier(trainSet);
		
		return trainedClassifier;
	}

	//TODO: This was the original evaluation in the SATD_detector tool. This can be replaced by the RulesZ classifier.
	public boolean isSATD(Instance toClassify) {

		String source = toClassify.stringValue(0); //TODO: Check that this is the correct value in your instances
		// identify specific tags
		if (RulesZ.containsNSATDTag(source))
			return false;
		if (RulesZ.containsSATDTag(source))
			return true;
		if (!RulesZ.isPossible(source))
			return false;
		if (RulesZ.containsSATDPatterns(source))
			return true;

		try {
			return !toClassify.classAttribute().value((int)trainedClassifier.classifyInstance(toClassify)).equals("WITHOUT_CLASSIFICATION"); 
		} catch (Exception e) {

		}
		return false;
	}
	
	public static void main(String[] args) throws Exception {

		ArffLoader loader = new ArffLoader();
		loader.setSource(new File("training.arff"));
		Instances training = loader.getDataSet();
		training.setClassIndex(training.numAttributes() - 1);

		loader = new ArffLoader();
		loader.setSource(new File("test.arff"));
		Instances test = loader.getDataSet();
		test.setClassIndex(test.numAttributes() - 1);
		
		SATDetector satd = new SATDetector();
		FilteredClassifier multiClass = satd.train(training); //training the multiclass classifier
		Evaluation eval = new Evaluation(training);
		eval.evaluateModel(multiClass, test);
		System.out.println(eval.toSummaryString());
		System.out.println(eval.toClassDetailsString());

		
		//Here the training and print of the evaluation. You can use this classifier as a normal Weka one.
		RulesZ rules = new RulesZ();
		rules.buildClassifier(training);
		Evaluation eval1 = new Evaluation(training);
		eval1.evaluateModel(rules, test);
		
		System.out.println(eval1.toSummaryString());
		System.out.println(eval1.toClassDetailsString());


	}

}
