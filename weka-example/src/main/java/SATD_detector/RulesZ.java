package main.java.SATD_detector;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import weka.attributeSelection.InfoGainAttributeEval;
import weka.attributeSelection.Ranker;
import weka.classifiers.AbstractClassifier;
import weka.classifiers.Sourcable;
import weka.classifiers.bayes.NaiveBayesMultinomial;
import weka.classifiers.meta.FilteredClassifier;
import weka.classifiers.rules.ZeroR;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.WeightedInstancesHandler;
import weka.core.stemmers.SnowballStemmer;
import weka.filters.Filter;
import weka.filters.MultiFilter;
import weka.filters.supervised.attribute.AttributeSelection;
import weka.filters.unsupervised.attribute.StringToWordVector;

public class RulesZ  extends AbstractClassifier implements WeightedInstancesHandler, Sourcable{

	FilteredClassifier fc;
	
	@Override
	public void buildClassifier(Instances data) throws Exception {
		StringToWordVector stw = new StringToWordVector(100000);
		stw.setOutputWordCounts(true);
		stw.setIDFTransform(true);
		stw.setTFTransform(true);
		SnowballStemmer stemmer = new SnowballStemmer();
		stw.setStemmer(stemmer);
		stw.setStopwordsHandler(new SATDStopwordsHandler());
		stw.setInputFormat(data);
		//		trainSet = Filter.useFilter(trainSet, stw);

		// attribute selection for training data
		AttributeSelection attSelection = new AttributeSelection();
		Ranker ranker = new Ranker();

		ranker.setNumToSelect(350);
		InfoGainAttributeEval ifg = new InfoGainAttributeEval();
		attSelection.setEvaluator(ifg);
		attSelection.setSearch(ranker);
		
//		attSelection.setInputFormat(trainSet);
		//		trainSet = Filter.useFilter(trainSet, attSelection);

		MultiFilter mf = new MultiFilter();
		mf.setFilters(new Filter[] {stw,attSelection});
		
		fc = new FilteredClassifier();
		fc.setFilter(mf);
		fc.setClassifier(new NaiveBayesMultinomial());
		
		fc.buildClassifier(data);
	}

	@Override
	public double classifyInstance(Instance instance) throws Exception {

		String source = instance.stringValue(0);
		if (containsNSATDTag(source))
			return instance.classAttribute().indexOfValue("WITHOUT_CLASSIFICATION");
		if (containsSATDTag(source))
			return instance.classAttribute().indexOfValue("SATD");
		if (!isPossible(source))
			return instance.classAttribute().indexOfValue("WITHOUT_CLASSIFICATION");
		if (containsSATDPatterns(source))
			instance.classAttribute().indexOfValue("SATD");

		return fc.classifyInstance(instance);
	}

	public static boolean isPossible(String source) {
		// ignore useless javadoc
		if (isJavadoc(source))
			return false;
		// ignore auto generated comments
		if (isAutoComment(source))
			return false;
		// ignore License comments
		if (isLicenseComment(source))
			return false;

		return true;
	}

	@Override
	public double[] distributionForInstance(Instance instance) throws Exception {

		String source = instance.stringValue(0);

		double [] dist = new double[instance.numClasses()];
		if (containsNSATDTag(source))
			dist[instance.classAttribute().indexOfValue("WITHOUT_CLASSIFICATION")]  = 1;
		else
			if (containsSATDTag(source))
				dist[instance.classAttribute().indexOfValue("SATD")]  = 1;
			else
				if (!isPossible(source))
					dist[instance.classAttribute().indexOfValue("WITHOUT_CLASSIFICATION")]  = 1;
				else
					if (containsSATDPatterns(source))
						dist[instance.classAttribute().indexOfValue("SATD")]  = 1;
					else
						dist[(int) fc.classifyInstance(instance)]  = 1;
		
		return dist;
	}

	public String toSource(String className) throws Exception {
		StringBuffer result;
		result = new StringBuffer();
		return result.toString();
	}

	public static boolean isJavadoc(String source) {
		String[] tags = { "todo", "fixme", "xxx" };
		source = source.toLowerCase();
		for (String tag : tags) {
			if (source.contains(tag))
				return false;
		}

		if (isDocComment(source))
			return true;

		return false;
	}

	public static boolean isDocComment(String source) {
		source = source.trim();
		if (source.startsWith("/**") && source.endsWith("*/"))
			return true;
		return false;
	}

	public static boolean isAutoComment(String source) {
		String pattern = ".*auto.*generated.*";
		source = source.toLowerCase();

		return Pattern.matches(pattern, source);
	}

	public static boolean isLicenseComment(String source) {
		String[] tags = { "license", "copyright", "all rights reserved" };
		source = source.toLowerCase();
		for (String tag : tags) {
			if (source.contains(tag))
				return true;
		}

		return false;
	}

	public static boolean containsSATDTag(String source) {
		// improvement: identify those comments with specific tags
		// do not use TODO, because auto-generated comments always start with TODO
		String pattern = ".*(([^N]|^)SATD).*";
		return Pattern.matches(pattern, source);
	}

	public static boolean containsNSATDTag(String source) {
		String[] tags = { "NSATD" };
		for (String tag : tags) {
			if (source.contains(tag)) {
				return true;
			}
		}

		return false;
	}

	public static boolean containsSATDPatterns(String source) {
		String pattern = ".*(TODO|FIXME|XXX).*";
		if (Pattern.matches(pattern, source))
			return true;
		source = source.toLowerCase();
		List<String> patterns = new ArrayList<>();
		patterns.add(".*(hack|nuke|yuck|hacky|fixme|kludge|give up|retarded|at a loss|this is bs|prolly a bug|fix this crap|is problematic|probably a bug|trial and error|get rid of this|certainly buggy|temporary crutch|abandon all hope|workaround for bug|this can be a mess|give up and go away|this isn’t very solid|this isn’t quite right|something bad happened|hang our heads in shame|risk of this blowing up|something bad is going on|treat this as a soft error|doubt that this would work|this is temporary and will go away|this indicates a more fundamental problem|ugly|barf|crap|silly|stupid|kaboom|toss it|bail out|take care|causes issue|this is wrong|inconsistency|don’t use this|this is uncool|cause for issue|just abandon it|remove this code|some fatal error|may cause problem|temporary solution|there is a problem|it doesn’t work yet|something’s gone wrong|is this next line safe|you can be unhappy now|this doesn’t look right|is this line really safe|hope everything will work|something serious is wrong|remove me before production|unknown why we ever experience this).*");
		for (String pat : patterns) {
			if (Pattern.matches(pat, source))
				return true;
		}

		return false;
	}
}
