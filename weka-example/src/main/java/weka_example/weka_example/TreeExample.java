package main.java.weka_example.weka_example;

import java.io.File;
import java.io.IOException;

import weka.classifiers.rules.JRip;
import weka.classifiers.trees.J48;
import weka.classifiers.trees.RandomForest;
import weka.classifiers.trees.j48.ClassifierTree;
import weka.core.Instances;
import weka.core.converters.ArffLoader;

public class TreeExample {

	public static void main(String[] args) throws Exception {

		ArffLoader loader = new ArffLoader();
		loader.setSource(new File("C:\\Users\\Anto\\Desktop\\contact-lenses.arff"));

		Instances instances = loader.getDataSet();
		instances.setClassIndex(instances.numAttributes() - 1);

		//		 System.out.println(instances);

		J48 j48 = new J48();
		j48.buildClassifier(instances);

		System.out.println(j48.toString());
		System.out.println("-----------------------");
		//		 System.out.println(j48.tree);
		System.out.println(j48.graph());
		System.out.println("-----------------------");
		System.out.println(j48.prefix());
		//		 System.out.println(j48.toSource(j48.prefix()));
		System.out.println("-----------------------");
		System.out.println("-----------------------");

		RandomForest rf = new RandomForest();
		rf.setPrintClassifiers(true);
		rf.buildClassifier(instances);
		System.out.println(rf.toString());

		System.out.println("-----------------------");
		System.out.println("-----------------------");

		JRip jrip = new JRip();
		jrip.buildClassifier(instances);
		System.out.println(jrip.toString());
		
	}

}
