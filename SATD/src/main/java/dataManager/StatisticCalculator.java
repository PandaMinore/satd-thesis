package dataManager;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.lang.Double;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

public class StatisticCalculator {
	
	public static void createHeader(List<String> header, List<Object> classes) {

		header.add("Combination");
		header.add("Correctly Classified Instances");
		header.add("% Correctly Classified Instances");
		header.add("Incorrectly Classified Instances");
		header.add("% Incorrectly Classified Instances");

		header.add("Weighted_TPRate");
		header.add("Weighted_FPRate");
		header.add("Weighted_Precision");
		header.add("Weighted_Recall");
		header.add("Weighted_F-Measure");

		for (Object cc : classes) {
			header.add(cc + "_TPRate");
			header.add(cc + "_FPRate");
			header.add(cc + "_Precision");
			header.add(cc + "_Recall");
			header.add(cc + "_F-Measure");
		}


	}
	
	public static void createMixedFM(double CM1 [][], double CM2 [][]) throws IOException {
		
		
		double numDebt = CM1[0][0] + CM1[0][1];
		
		double numNonDebt = CM1[1][0] + CM1[1][1];
		double numTest = CM2[0][0] + CM2[0][1] + CM2[0][2] + CM2[0][3] + CM2[0][4];
		double numReq = CM2[1][0] + CM2[1][1] + CM2[1][2] + CM2[1][3] + CM2[1][4];
		double numDes = CM2[2][0] + CM2[2][1] + CM2[2][2] + CM2[2][3] + CM2[2][4];
		double numDef = CM2[3][0] + CM2[3][1] + CM2[3][2] + CM2[3][3] + CM2[3][4];
		double numDocu = CM2[4][0] + CM2[4][1] + CM2[4][2] + CM2[4][3] + CM2[4][4];
		
		double numTot = numDebt + numNonDebt;
		
		// TP, FN, TN, FP
		double[] nonSATD = {CM1[1][1], CM1[1][0], numDebt, 0};
		
		double FN = numTest - CM2[0][0];
		double FP = CM2[1][0] + CM2[2][0] + CM2[3][0] + CM2[4][0];
		double[] test = {CM2[0][0], FN, numTot - FN - FP - CM2[0][0], FP};
		
		FN = numReq - CM2[1][1];
		FP = CM2[0][1] + CM2[2][1] + CM2[3][1] + CM2[4][1];
		double[] requirement = {CM2[1][1], FN, numTot - FN - FP - CM2[1][1], FP};
		
		FN = numDes - CM2[2][2];
		FP = CM2[0][2] + CM2[1][2] + CM2[3][2] + CM2[4][2];
		double[] design = {CM2[2][2], FN, numTot - FN - FP - CM2[2][2], FP};
		
		FN = numDef - CM2[3][3];
		FP = CM2[0][3] + CM2[1][3] + CM2[2][3] + CM2[4][3];
		double[] defect = {CM2[3][3], FN, numTot - FN - FP - CM2[3][3], FP};
		
		FN = numDocu - CM2[4][4];
		FP = CM2[0][4] + CM2[1][4] + CM2[2][4] + CM2[3][4];
		double[] documentation = {CM2[4][4], FN, numTot - FN - FP - CM2[4][4], FP};
		double[][] confusionMatrices = {nonSATD , test, requirement, design, defect, documentation};
		
		double numInst[] = {numNonDebt, numTest, numReq, numDes, numDef, numDocu, numTot};
		
		calculateStatistics(confusionMatrices, numInst);
		
	}
	
	public static void calculateStatistics(double CM[][], double numInst[]) throws IOException {
		
		boolean firstTime = false;
		
		double correct = 0, incorrect = 0;
		double[] TPR = new double[6], FPR = new double[6], precision = new double[6], recall = new double[6], Fmeasure = new double[6]; 
		
		for ( int i = 0; i < CM.length; i++) {
			
			double[] k = CM[i];
			TPR[i] = k[0] / (k[0] + k[1]);
			FPR[i] = k[3] / (k[3] + k[2]);
			precision[i] = k[0] / (k[0] + k[3]);
			
			Double pre = precision[i];
			if (pre.isNaN()) {
				precision[i] = 0;
			}
			
			recall[i] = k[0] / (k[0] + k[1]);
			Fmeasure[i] = 2*(precision[i]*recall[i]) / (precision[i] + recall[i]);
			
			Double Fm = Fmeasure[i];
			if (Fm.isNaN()) {
				Fmeasure[i] = 0;
			}
			
			correct += k[0];
			
		}
		
		incorrect = numInst[6] - correct;
		
		double weightedTPR = ((TPR[0] * numInst[0]) + (TPR[1] * numInst[1]) + (TPR[2] * numInst[2]) + (TPR[3] * numInst[3]) + (TPR[4] * numInst[4]) + (TPR[5] * numInst[5])) / (numInst[6]);
		double weightedFPR = ((FPR[0] * numInst[0]) + (FPR[1] * numInst[1]) + (FPR[2] * numInst[2]) + (FPR[3] * numInst[3]) + (FPR[4] * numInst[4]) + (FPR[5] * numInst[5])) / (numInst[6]);
		double weightedPrecision = ((precision[0] * numInst[0]) + (precision[1] * numInst[1]) + (precision[2] * numInst[2]) + (precision[3] * numInst[3]) + (precision[4] * numInst[4]) + (precision[5] * numInst[5])) / (numInst[6]);
		double weightedRecall = ((recall[0] * numInst[0]) + (recall[1] * numInst[1]) + (recall[2] * numInst[2]) + (recall[3] * numInst[3]) + (recall[4] * numInst[4]) + (recall[5] * numInst[5])) / (numInst[6]);
		double weightedFmeasure = ((Fmeasure[0] * numInst[0]) + (Fmeasure[1] * numInst[1]) + (Fmeasure[2] * numInst[2]) + (Fmeasure[3] * numInst[3]) + (Fmeasure[4] * numInst[4]) + (Fmeasure[5] * numInst[5])) / (numInst[6]);
		
		List<Object> classes = new ArrayList<Object>();
		classes.add("nonSATD");
		classes.add("Test");
		classes.add("Requirement");
		classes.add("Design");
		classes.add("Defect");
		classes.add("Documentation");
		
		List<Object> results = new ArrayList<>();
		
		
		results.add("DebtHunter_Raw");
		results.add(correct);
		results.add(correct * 100 / numInst[6]);
		results.add(incorrect);
		results.add(incorrect * 100 / numInst[6]);
		results.add(weightedTPR);
		results.add(weightedFPR);
		results.add(weightedPrecision);
		results.add(weightedRecall);
		results.add(weightedFmeasure);


		for (int i = 0; i < classes.size(); i++) {
			results.add(TPR[i]);
			results.add(FPR[i]);
			results.add(precision[i]);
			results.add(recall[i]);
			results.add(Fmeasure[i]);

		}

		
		
		if(firstTime) {
			
			List<String> header = new ArrayList<>();
			createHeader(header, classes);
			
			BufferedWriter writer = Files.newBufferedWriter(Paths.get("2step_mixed_statistic.csv"));
			CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT.withHeader(header.toArray(new String[header.size()])));
			csvPrinter.printRecord(results);
			csvPrinter.close();
			writer.close();
			
		} else {
			
			FileWriter csv = new FileWriter("2step_mixed_statistic.csv", true);
			BufferedWriter writer = new BufferedWriter(csv);
			
			String tmp = "";
			for (Object element : results) {
				tmp += element + ",";
			}

			tmp = tmp.substring(0, tmp.length() - 1);
			
			writer.write(tmp);
			writer.newLine();
			writer.close();
			
		}
		

	}

}
