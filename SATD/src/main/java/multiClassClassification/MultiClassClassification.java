package multiClassClassification;

import java.util.Arrays;
import java.util.Map;

import dataManager.DataHandler;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.bayes.NaiveBayesMultinomial;
import weka.classifiers.functions.LibSVM;
import weka.classifiers.functions.SMO;
import weka.classifiers.functions.supportVector.RBFKernel;
import weka.classifiers.meta.FilteredClassifier;
import weka.core.Instances;


public class MultiClassClassification {

	public static void main(String[] args) throws Exception {


		//upload original csv, data cleaning, split data into training and test set
		String path = "C:/Users/irene/eclipse-workspace/SATD/datasets/technical_debt_dataset.csv";
		
		Map<String,String> comments = DataHandler.loadFile(path); 
		Instances instances	= DataHandler.creatingArff(comments);
		 
		// divide 70-30 with instances chooses at random 
		Instances [] data = DataHandler.divideRandom(instances);
		System.out.println(instances.numInstances() + " " + data[0].numInstances() +
		" " + data[1].numInstances()); // data, training, test
		DataHandler.saveData(data[0], "training", data[1], "test");
		Instances training = data[0];
		Instances test = data[1];
		
	
		// classifiers to consider for classification task
		NaiveBayesMultinomial nbm = new NaiveBayesMultinomial();

		// radial basis function: exp(-gamma*|u-v|^2)
		LibSVM libSVM = new LibSVM();
		libSVM.setSeed(8);
		libSVM.setGamma(0.0);
		libSVM.setCost(1.0);

		SMO smo = new SMO();
		smo.setRandomSeed(8);
		RBFKernel kernel = new RBFKernel();
		smo.setKernel(kernel);

//		AdaBoostM1 adaBoost = new AdaBoostM1();

		Classifier[] classificationModel = { nbm }; //nbm, smo, adaBoost
		String[] classifierName = {  "NaiveBayesMultinomial" }; // "NaiveBayesMultinomial", "SMO", "AdaBoost" 

		// feature extracted = 13,057
		String preCombination = "_SpreadSubsample10_WordTokenizer_customStopwords_LovinsStemmer_no";

		Boolean firstTime = false; // true if is the first time it runs

		for (int i = 0; i < classificationModel.length; i++) {

			Classifier classifier = classificationModel[i];

			// perform the grid search for classifiers that have parameters to be optimized.
			LibSVM lib = new LibSVM();
			SMO smoIn = new SMO();
			
			if (lib.getClass().isInstance(classifier)) {
				classifier = (LibSVM) Classification.parametersOptimization(training, classifier, classifierName[i]);
			} else if (smoIn.getClass().isInstance(classifier)) {
				classifier = (SMO) Classification.parametersOptimization(training, classifier, classifierName[i]);
			}

			// 10 fold CV
			double[] fMeasure = Classification.tenFoldCV(training, classifier, classifierName[i]);
			System.out.println(Arrays.toString(fMeasure));

			// evaluate model with test set
			FilteredClassifier fc = Classification.train(training, classifier);
			Evaluation eval = new Evaluation(training);
			eval.evaluateModel(fc, test);
			System.out.println(eval.toSummaryString());
			System.out.println(eval.toClassDetailsString());

			String combination = classifierName[i] + preCombination;
			if (firstTime) {

				Classification.evaluatorFirst(fc, eval, training, fMeasure, combination);
				firstTime = false;

			} else {

				Classification.evaluator(fc, eval, training, fMeasure, combination);

			}


		}


	}

}
