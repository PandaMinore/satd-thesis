package patternClassification;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import dataManager.DataHandler;
import dataManager.FeaturesHandler;
import weka.core.Instance;
import weka.core.Instances;

public class patternClassification {

	public static Instances classify(Instances data) {
		

		String[] patterns = { "hack", "ugly", "nuke", "barf", "yuck", "crap", "hacky", "silly", "fixme", "stupid",
				"kludge", "kaboom", "give up", "toss it", "retarded", "bail out", "at a loss", "take care",
				"this is bs", "causes issue", "probably a bug", "this is wrong", "fix this crap", "inconsistency",
				"is problematic", "do not use this", "this is uncool", "trial and error", "cause for issue",
				"get rid of this", "just abandon it", "certainly buggy", "remove this code", "temporary crutch",
				"some fatal error", "abandon all hope", "may cause problem", "workaround for bug", "temporary solution",
				"this can be a mess", "there is a problem", "give up and go away", "it does not work yet",
				"this is not very solid", "something is gone wrong", "this is not quite right",
				"is this next line safe", "something bad happened", "you can be unhappy now", "hang our heads in shame",
				"this does not look right", "risk of this blowing up", "is this line really safe",
				"something bad is going on", "hope everything will work", "treat this as a soft error",
				"something serious is wrong", "doubt that this would work", "remove me before production",
				"this is temporary and will go away", "unknown why we ever experience this",
				"this indicates a more fundamental problem" }; // probably a bug = prolly a bug (for data cleaning)
		
		for (int i = 0; i < data.numInstances(); i++) {
			
			String comment = data.instance(i).stringValue(0);
				
			boolean flag = Arrays.stream(patterns).anyMatch(comment::contains);
			
/*			Double SATDclass = newTest.instance(i).value(1);
			
			if (SATDclass != 2.0) {
				System.out.println(newTest.instance(i) + " " + newTest.instance(i).value(1) + " " + newTest.instance(i).value(2));
			}
*/			
			
			if (flag) {
				data.instance(i).setValue(data.numAttributes() - 1, "SATD");
			} else {
				data.instance(i).setValue(data.numAttributes() - 1, "WITHOUT_CLASSIFICATION");
			}
			
		}
		
		return data;
		
	}
	
	public static void createHeader(List<String> header, List<Object> classes) {
		
		header.add("Configuration");
		header.add("Correctly Classified Instances");
		header.add("% Correctly Classified Instances");
		header.add("Incorrectly Classified Instances");
		header.add("% Incorrectly Classified Instances");

		header.add("Weighted_TP Rate");
		header.add("Weighted_FP Rate");
		header.add("Weighted_Precision");
		header.add("Weighted_Recall");
		header.add("Weighted_F-Measure");

		for (Object cc : classes) {
			header.add(cc + "_TP Rate");
			header.add(cc + "_FP Rate");
			header.add(cc + "_Precision");
			header.add(cc + "_Recall");
			header.add(cc + "_F-Measure");
		}


	}
	
	public static void evaluator(Instances data, String name, boolean flag, int[]numAttr) throws IOException {
		
		List<Object> classes = Collections.list(data.classAttribute().enumerateValues());
		List<String> header = new ArrayList<>();
		createHeader(header, classes);
		List<Object> results = new ArrayList<>();

		double correct = 0, incorrect = 0;
		double TP = 0, FP = 0, FN = 0, TN = 0;
		double numDebt = numAttr[0], numNotDebt = numAttr[1];
		
		for (int i = 0; i < data.numInstances(); i++) {
			
			Double correctClass = data.instance(i).value(1);
			Double patternClass = data.instance(i).value(2);
			
			// correct: non-SATD = 2.0, DEFECT = 4.0, DESIGN = 3.0, REQ = 1.0, TEST = 0.0, DOCU = 5.0
			// pattern: non-SATD = 1.0, SATD = 0.0
			boolean checkFN = (patternClass == 1.0) && ((correctClass == 0.0) ||(correctClass == 1.0) || (correctClass == 3.0) || (correctClass == 4.0) || (correctClass == 5.0));
			boolean checkFP = (patternClass == 0.0) && (correctClass == 2.0);
			boolean checkTP = (patternClass == 0.0) && ((correctClass == 0.0) ||(correctClass == 1.0) || (correctClass == 3.0) || (correctClass == 4.0) || (correctClass == 5.0));
			boolean checkTN = (patternClass == 1.0) && (correctClass == 2.0);
			
			if (checkFN || checkFP) {
					incorrect ++; // FN + FP of both classes without distinction
				
				if (checkFP) {
					FP ++;
				} else if (checkFN) {
					FN ++;
				}
				
			} else if (checkTP || checkTN) {
				correct ++;  // TP + TN
				
				if (checkTP) {
					TP ++;
				} else if (checkTN) {
					TN ++;
					
				}
			}
			
		}
		
		System.out.println("SATD correctly classified: " + TP + "  SATD incorrectly classified:" + FN);
		System.out.println("nonSATD correctly classified: " + TN + "  nonSATD incorrectly classified:" + FP);
		
		double TPRdebt = TP / (TP + FN);
		double TPRnonDebt = TN / (TN + FP);
		double weightedTPR = ((TPRdebt * numDebt) + (TPRnonDebt * numNotDebt)) / (numDebt + numNotDebt);
		
		double FPRdebt = FP / (FP + TN);
		double FPRnonDebt = FN / (FN + TP);
		double weightedFPR = ((FPRdebt * numDebt) + (FPRnonDebt * numNotDebt)) / (numDebt + numNotDebt);
		
		double precisionDebt = TP / (TP + FP);
		double precisionNonDebt = TN / (TN + FN);
		double weightedPrecision = ((precisionDebt * numDebt) + (precisionNonDebt * numNotDebt)) / (numDebt + numNotDebt);
		
		double recallDebt = TP / (TP + FN);
		double recallNonDebt = TN / (TN + FP);
		double weightedRecall = ((recallDebt * numDebt) + (recallNonDebt * numNotDebt)) / (numDebt + numNotDebt);
		
		double FmeasureDebt = 2*(precisionDebt*recallDebt) / (precisionDebt + recallDebt);
		double FmeasureNonDebt = 2*(precisionNonDebt*recallNonDebt) / (precisionNonDebt + recallNonDebt);
		double weightedFmeasure = ((FmeasureDebt * numDebt) + (FmeasureNonDebt * numNotDebt)) / (numDebt + numNotDebt);
		
		System.out.println("precisionDebt: " + precisionDebt + "  recallDebt: " + recallDebt + "  FmeasureDebt: " + FmeasureDebt);
		
		results.add(name);
		results.add(correct);
		results.add(correct * 100 / data.numInstances());
		results.add(incorrect);
		results.add(incorrect * 100 / data.numInstances());
		results.add(weightedTPR);
		results.add(weightedFPR);
		results.add(weightedPrecision);
		results.add(weightedRecall);
		results.add(weightedFmeasure);
		
		//debt
		results.add(TPRdebt);
		results.add(FPRdebt);
		results.add(precisionDebt);
		results.add(recallDebt);
		results.add(FmeasureDebt);
		
		//non-debt
		results.add(TPRnonDebt);
		results.add(FPRnonDebt);
		results.add(precisionNonDebt);
		results.add(recallNonDebt);
		results.add(FmeasureNonDebt);
		
		
		if (flag) {
			
			BufferedWriter writer = Files.newBufferedWriter(Paths.get("output_pattern.csv"));
			CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT.withHeader(header.toArray(new String[header.size()])));
			csvPrinter.printRecord(results);
			csvPrinter.close();
			writer.close();
			
		} else {
			
			FileWriter csv = new FileWriter("output_pattern.csv", true);
			BufferedWriter writer = new BufferedWriter(csv);
			
			String tmp = "";
			for (Object element : results) {
				tmp += element + ",";
			}

			tmp = tmp.substring(0, tmp.length() - 1);

			writer.write(tmp);
			writer.newLine();
			writer.close();

		}
	
	}
	
	public static void main(String[] args) throws Exception {
		
		// create test set
		String path = "C:/Users/irene/eclipse-workspace/SATD/datasets/technical_debt_dataset.csv";

		Map<String, String> comments = DataHandler.loadFile(path);
		Instances instances = DataHandler.creatingArff(comments);
		
		Instances[] data = DataHandler.divideRandom(instances);
		Instances training = data[0];
		Instances test = data[1];
		
		// add a new empty feature (will be the pattern-based approach's labels)
		Instances newTest = FeaturesHandler.Binarization(test, false);
		newTest.setClassIndex(newTest.numAttributes() - 1);
		Instances newTraining = FeaturesHandler.Binarization(training, false);
		newTest.setClassIndex(newTraining.numAttributes() - 1);
		
		
		// set BinaryClassification value into "SATD" if at least one patter is in the code comment
		newTest = classify(newTest);
		newTraining = classify(newTraining);
		
		DataHandler.saveOneData(newTest, "patternTest");
		DataHandler.saveOneData(newTraining, "patternTraining");
		
		int[] numAttr = {1012, 10019};
		
		evaluator(newTest, "pattern", true, numAttr);
		
		
		
		// simulate the 10 fold CV
		for (int i = 1; i <= 10; i++) {
			
			System.out.println("iteration " + (i));
			Instances[] fold = DataHandler.divideStratifiedInstances(newTraining, i);
			
			int[] countAttributes = new int[fold[1].attribute(1).numValues()];
			for(Instance inst: fold[1]){
			    countAttributes[(int)inst.value(2)]++;
			}
			
			System.out.println(Arrays.toString(countAttributes));
			
			
			evaluator(fold[1], "fold-test_" + i, false, countAttributes);
			
		}	

	}

}
