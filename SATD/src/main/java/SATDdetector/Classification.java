package SATDdetector;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import dataManager.DataHandler;

import weka.classifiers.Evaluation;
import weka.classifiers.meta.FilteredClassifier;
import weka.core.Instances;

public class Classification {
	
	public static double[] tenFoldCV (Instances training, int [] nFeature) throws Exception {
		
		double[] fMeasure = new double[11];
		for(int i = 1; i <= 10; i++) {
			
			// divide training into 10-fold
			Instances [] fold = DataHandler.divideStratifiedInstances(training, i);
			SATDetector satd = new SATDetector();
			FilteredClassifier CVClassifier = satd.train(fold[0], nFeature[i-1]);
			
/*			RulesZ CVClassifier = new RulesZ();
			CVClassifier.buildClassifier(fold[0]);
*/			
			Evaluation eval = new Evaluation(fold[0]);
			eval.evaluateModel(CVClassifier, fold[1]);
			fMeasure[i-1] = eval.weightedFMeasure();
			
		}
		
		fMeasure[10] = (fMeasure[0] + fMeasure[1] + fMeasure[2] + fMeasure[3] + fMeasure[4] + fMeasure[5] + fMeasure[6] + fMeasure[7] + fMeasure[8] + fMeasure[9])/10 ;
		System.out.println("The mean weighted Fmeasure for the 10 fold CV is " + fMeasure[10]);
		return fMeasure;
	
	}

	//first time we add statistics into csv
	public static void evaluatorFirst(Evaluation eval, Instances instances, double[] Fmeasures, String combination, String type) throws Exception {
		
		List<Object> classes = Collections.list(instances.classAttribute().enumerateValues());
		
		List<String> header = new ArrayList<>();
		createHeader(header, classes);
		
		List<Object> results = new ArrayList<>();
		results.add(combination);
		createResults(results, eval, classes, Fmeasures);
		
		BufferedWriter writer = Files.newBufferedWriter(Paths.get("output_SATDdetector_" + type + ".csv"));
		CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT.withHeader(header.toArray(new String[header.size()])));
		csvPrinter.printRecord(results);
		csvPrinter.close();
		writer.close();
		
	}
	
	// classifiers evaluator
	public static void evaluator(Evaluation eval, Instances instances, double[] Fmeasures, String combination, String type) throws Exception {
		
		FileWriter csv = new FileWriter ("output_SATDdetector_" + type + ".csv", true);
		BufferedWriter writer = new BufferedWriter(csv);
		
		List<Object> classes = Collections.list(instances.classAttribute().enumerateValues());
		
		List<Object> results = new ArrayList<>();
		results.add(combination);
		createResults(results, eval, classes, Fmeasures);
		
		String tmp = "";
		for (Object element: results) {
			tmp += element + ",";
		}
		
		tmp = tmp.substring(0, tmp.length() - 1);
		
		writer.write(tmp);
		writer.newLine();
		writer.close();
		
	}
	
	public static void createHeader (List<String> header, List<Object> classes) {
		
		header.add("combination");
		header.add("Correctly Classified Instances");
		header.add("% Correctly Classified Instances");
		header.add("Incorrectly Classified Instances");
		header.add("% Incorrectly Classified Instances");
		
		header.add("Weighted_TP Rate");
		header.add("Weighted_FP Rate");
		header.add("Weighted_Precision");
		header.add("Weighted_Recall");
		header.add("Weighted_F-Measure");
		header.add("Weighted_ROC Area");
		header.add("Weighted_PRC Area");
		
		for(Object cc : classes) {
			header.add(cc+"_TP Rate");
			header.add(cc+"_FP Rate");
			header.add(cc+"_Precision");
			header.add(cc+"_Recall");
			header.add(cc+"_F-Measure");
			header.add(cc+"_ROC Area");
			header.add(cc+"_PRC Area");
		}
		
		header.add("Mean_Weighted_F-Measure");
		
	}
	
	public static void createResults (List<Object> results, Evaluation eval, List<Object> classes, double[] fm) {
		
		results.add(eval.correct());
		results.add(eval.correct() * 100 / eval.numInstances());
		results.add(eval.incorrect());
		results.add(eval.incorrect() * 100 / eval.numInstances());
		results.add(eval.weightedTruePositiveRate());
		results.add(eval.weightedFalsePositiveRate());
		results.add(eval.weightedPrecision());
		results.add(eval.weightedRecall());
		results.add(eval.weightedFMeasure());
		results.add(eval.weightedAreaUnderROC());
		results.add(eval.weightedAreaUnderPRC());
		
		for(int i = 0; i < classes.size(); i++) {
			results.add(eval.truePositiveRate(i));
			results.add(eval.falsePositiveRate(i));
			results.add(eval.precision(i) );
			results.add(eval.recall(i));
			results.add(eval.fMeasure(i));
			results.add(eval.areaUnderROC(i));
			results.add(eval.areaUnderPRC(i));
		}
		
		results.add(fm[10]);
		
	}
	
}
