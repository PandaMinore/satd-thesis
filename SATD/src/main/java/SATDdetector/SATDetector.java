package SATDdetector;

import java.io.File;
import java.util.Arrays;

import weka.attributeSelection.InfoGainAttributeEval;
import weka.attributeSelection.Ranker;
import weka.classifiers.Evaluation;
import weka.classifiers.bayes.NaiveBayesMultinomial;
import weka.classifiers.meta.FilteredClassifier;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ArffLoader;
import weka.core.stemmers.SnowballStemmer;
import weka.core.stopwords.WordsFromFile;
import weka.filters.Filter;
import weka.filters.MultiFilter;
import weka.filters.supervised.attribute.AttributeSelection;
import weka.filters.unsupervised.attribute.StringToWordVector;

public class SATDetector {
	
	double ratio = 0.1;
	static FilteredClassifier trainedClassifier;
	
	public FilteredClassifier train(Instances trainSet, int nFeature) throws Exception {

		WordsFromFile stopwords = new WordsFromFile();
		stopwords.setStopwords(new File("/dic/stopWords.txt"));
		
		StringToWordVector stw = new StringToWordVector(100000);
		stw.setOutputWordCounts(true);
		stw.setTFTransform(true);
		stw.setIDFTransform(true);
		SnowballStemmer stemmer = new SnowballStemmer();
		stw.setStemmer(stemmer);
		stw.setStopwordsHandler(new SATDStopwordsHandler());
		stw.setInputFormat(trainSet);

		// attribute selection for training data
		AttributeSelection attSelection = new AttributeSelection();
		Ranker ranker = new Ranker();
		
		ranker.setNumToSelect(nFeature);
//		ranker.setNumToSelect((int) (trainSet.numAttributes() * ratio));
//		System.out.println((int) (trainSet.numAttributes()) + " " + (int) (trainSet.numAttributes() * ratio));
		InfoGainAttributeEval ifg = new InfoGainAttributeEval();
		attSelection.setEvaluator(ifg);
		attSelection.setSearch(ranker);

		MultiFilter mf = new MultiFilter();
		mf.setFilters(new Filter[] {stw, attSelection});
		
		trainedClassifier = new FilteredClassifier();
		trainedClassifier.setFilter(mf);
		trainedClassifier.setClassifier(new NaiveBayesMultinomial());
		
		trainedClassifier.buildClassifier(trainSet);
		
		return trainedClassifier;
		
	}
	
	// This was the original evaluation in the SATD_detector tool. This can be replaced by the RulesZ classifier.
	public static boolean isSATD(Instance toClassify) {

		String source = toClassify.stringValue(0);
		// identify specific tags
		if (RulesZ.containsNSATDTag(source))
			return false;
		if (RulesZ.containsSATDTag(source))
			return true;
		if (!RulesZ.isPossible(source))
			return false;
		if (RulesZ.containsSATDPatterns(source))
			return true;

		try {
			return !toClassify.classAttribute().value((int)trainedClassifier.classifyInstance(toClassify)).equals("WITHOUT_CLASSIFICATION"); 
		} catch (Exception e) {

		}
		return false;
	}

	
	public static void main(String[] args) throws Exception {

		ArffLoader loader = new ArffLoader();
		loader.setSource(new File("datasets/training.arff"));
		Instances training = loader.getDataSet();
		training.setClassIndex(training.numAttributes() - 1);

		loader = new ArffLoader();
		loader.setSource(new File("datasets/test.arff"));
		Instances test = loader.getDataSet();
		test.setClassIndex(test.numAttributes() - 1);
			
		loader = new ArffLoader();
		loader.setSource(new File("datasets/training_binary.arff"));
		Instances binTraining = loader.getDataSet();
		binTraining.setClassIndex(binTraining.numAttributes() - 1);

		loader = new ArffLoader();
		loader.setSource(new File("datasets/test_binary.arff"));
		Instances binTest = loader.getDataSet();
		binTest.setClassIndex(binTest.numAttributes() - 1);		

		String combination = "AllData_NaiveBayesMultinomial_WordTokenizer_SATDstopwords_SnowballStemmer_10%";
		
		Boolean firstTime = true; // true if is the first time it runs 
		double[] fmMulti = new double [11];
		double[] fmBinary = new double [11];
		
		int[] nFeatureMulti = {1556, 1562, 1556, 1554, 1553, 1560, 1555, 1549, 1564, 1564, 1656};
		int[] nFeatureBinary = {1556, 1565, 1557, 1553, 1552, 1560, 1558, 1548, 1560, 1565, 1656};
		
		// 10 fold CV 
		fmMulti = Classification.tenFoldCV(training, nFeatureMulti);; 
		
		// MULTI (all classes): evaluate model with test set 
		SATDetector satd = new SATDetector();
		FilteredClassifier multiClass = satd.train(training, nFeatureMulti[10]); 
		Evaluation eval = new Evaluation(training); 
		eval.evaluateModel(multiClass, test);
		System.out.println(eval.toSummaryString());
		System.out.println(eval.toClassDetailsString());
		 
		if (firstTime) {
			
			Classification.evaluatorFirst(eval, training, fmMulti, combination, "multi");
			
		} else {
			
			Classification.evaluator(eval, training, fmMulti, combination, "multi");
		
		}
		
		// 10 fold CV
		fmBinary = Classification.tenFoldCV(binTraining, nFeatureBinary);
		System.out.println(Arrays.toString(fmBinary));
		
		// BINARY: evaluate model with test set
		SATDetector satd1 = new SATDetector();
		FilteredClassifier binary = satd1.train(binTraining, nFeatureBinary[10]); 
		Evaluation eval1 = new Evaluation(binTraining); 
		eval1.evaluateModel(binary, binTest);
		System.out.println(eval1.toSummaryString());
		System.out.println(eval1.toClassDetailsString());

		if (firstTime) {
			
			Classification.evaluatorFirst(eval1, binTraining, fmBinary, combination, "binary");
			
		} else {
			
			Classification.evaluator(eval1, binTraining, fmBinary, combination, "binary");
			
		} 
		
		System.out.println("[TP, FN], [FP, TN]");
		System.out.println(Arrays.deepToString(eval1.confusionMatrix()));

	}

}
