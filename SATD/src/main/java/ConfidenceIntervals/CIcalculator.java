package ConfidenceIntervals;

import java.io.IOException;

public class CIcalculator {

	public static void main(String[] args) throws IOException {
		
//		double numInst = 11031, numInstSATD = 1012;
		
		
		// RQ1.1
		double[] binary = {};
		double[] multi = {0.9460591959784396, 0.9464218311839684, 0.9442959365760086, 0.9468995690080997, 0.9424589474690014, 0.9437257329375943, 0.9415396247821498, 0.9455555749848047, 0.948332470037313, 0.9423567998555448};
		
		String[] classifierNames1 = {"Binary", "Multi"};
		double[][] allFM1 = {binary, multi};
//		double[] F11 = {0.833, 0.947};
//		double[] numInst1 = {numInst, numInst};

		StatisticsTests.compareResult(allFM1, classifierNames1, true);
//		StatisticsTests.compareResult2(F11, numInst1, classifierNames1, true);
		
		
		//RQ1.2
		double[] OurRaw = {0.969926023765388, 0.964983164983164, 0.970524400812862, 0.971115840681058, 0.980332167832167, 0.973886207689024, 0.972545972545972, 0.981669033393171, 0.978100284134766, 0.978868957129826};
		double[] step1raw = {};
		double[] step2raw = {0.6586376108805384, 0.6922961766482293, 0.7223085462280194, 0.6800405857534918, 0.7509405671012649, 0.710086458669609, 0.6984872621689747, 0.6898643277161837, 0.7111291827375894, 0.6829166100466549};		
		
		String[] classifierNames2 = {"OurRaw", "1stStep", "2ndStep"};
		double[][] allFM2 = {OurRaw, step1raw, step2raw};
//		double[] F12 = {0.965, 0.833, 0.661};
//		double[] numInst2 = {numInst, numInst, numInstSATD};
		
		StatisticsTests.compareResult(allFM2, classifierNames2, false);
//		StatisticsTests.compareResult2(F12, numInst2, classifierNames2, false);

		
		
		//RQ1.3
		double[] twoNBM = {0.94925936634239, 0.951764414052642, 0.948725798127899, 0.94643313857041, 0.944757133617665, 0.949080496945323, 0.942183479642192, 0.947818576320424, 0.952479041187718, 0.948836566070338};
		double[] twoLSVM = {0.968662900672434, 0.969169194878992, 0.965204666719298, 0.96711398698505, 0.970470296914898, 0.962844141311135, 0.967051570404178, 0.969900851490013, 0.966796162939441, 0.970096718046422};
		double[] twoAB = {0.958635068054555, 0.959484020852991, 0.960647948612097, 0.958620376013162, 0.959799557306794, 0.958349380078771, 0.959888180976979, 0.960970277278977, 0.96104721603102, 0.959167374008802};
		double[] NLP = {0.9334454710970838, 0.9277573104188835, 0.9342848479792691, 0.9355292614876265, 0.926366738029805, 0.9322632497263885, 0.9336460669620954, 0.9394292175999838, 0.9266013495787141, 0.9331124322936045};
		double[] PB = {0.0829875518672199, 0.0703125, 0.0934579439252336, 0.112068965517241, 0.0999999999999999, 0.094017094017094, 0.122807017543859, 0.0948275862068965, 0.072289156626506, 0.0944206008583691};
				
		String[] classifierNames3 = {"OurRaw", "2NBM", "2LSVM", "2AB", "NLP", "PB"};
		double[][] allFM3 = {OurRaw, twoNBM, twoLSVM, twoAB, NLP, PB};
//		double[] F13 = {0.965, 0.949, 0.966, 0.959, 0.662, 0.098};
//		double[] numInst3 = {numInst, numInst, numInst, numInst, numInst, numInst};
		
		StatisticsTests.compareResult(allFM3, classifierNames3, false);
//		StatisticsTests.compareResult2(F13, numInst3, classifierNames3, false);
		
		
		//RQ2 (step1)
		double[] raw200_step1 = {};
		double[] raw300_step1 = {};
		double[] raw400_step1 = {};
		double[] raw500_step1 = {};
		double[] raw750_step1 = {};
		double[] raw1k_step1 = {};
		double[] raw1k5_step1 = {};
		double[] raw2k_step1 = {};
		double[] raw2k5_step1 = {};
		double[] raw3k_step1 = {};
		double[] raw5k_step1 = {};
		double[] raw7k5_step1 = {};
		double[] raw10k_step1 = {};
		//RQ2 (step2)
		double[] raw200_step2 = {0.6256019348737215, 0.6245121671258033, 0.6256678157720742, 0.607810178236519, 0.669580726351052, 0.6424351440795764, 0.598612623900024, 0.6492398850561029, 0.668413816483317, 0.6897168070983629};
		double[] raw300_step2 = {0.6266266576592416, 0.6207373493314295, 0.6422256795094721, 0.5935396245846353, 0.6567978367847366, 0.6374822713474414, 0.6163994075992113, 0.6412904354026172, 0.6483169297070642, 0.6747767553821364};
		double[] raw400_step2 = {0.6369006473557165, 0.6118418801225125, 0.6609445112251581, 0.5863273447217447, 0.6652901534159965, 0.6358814280159889, 0.6250278099850052, 0.6431300045044743, 0.6306269113035807, 0.6601692776580669};
		double[] raw500_step2 = {0.6417159620933884, 0.6140543592018747, 0.6535700291135231, 0.5959892050393477, 0.6745001903860723, 0.6481773014308078, 0.6123604787784344, 0.6486103151517533, 0.6306269113035807, 0.6382888153203155};
		double[] raw750_step2 = {0.6095930877006296, 0.6111642808393982, 0.6502495535378768, 0.6046289018070105, 0.6853051095833563, 0.6437595744088238, 0.6233268566977866, 0.6360589584804831, 0.6291908039204267, 0.6440992993866077};
		double[] raw1k_step2 = {0.6159533648170011, 0.5847826024970344, 0.5838928614713368, 0.5834440488170614, 0.6093549280515472, 0.6178979837598378, 0.5963667381555859, 0.6174582368304341, 0.6115919954484976, 0.6256346588592818};
		double[] raw1k5_step2 = {0.642759771456024, 0.6264841551301302, 0.5857106003099136, 0.5920631508727717, 0.6318317208451738, 0.6233549297628859, 0.6003652610957018, 0.6540874360142418, 0.6379820054801804, 0.6139749262411959};
		double[] raw2k_step2 = {0.6592033208922797, 0.6162719126111982, 0.6251999257336719, 0.5894902362459283, 0.6401597157202539, 0.6325989257379392, 0.6043826885893352, 0.6721862752851389, 0.6478917393842735, 0.6185655978494403};
		double[] raw2k5_step2 = {0.6595821865189901, 0.6147657014310741, 0.6165019539612011, 0.5983693603906516, 0.6315375974845231, 0.6258601600805526, 0.6243945519469414, 0.6707405973244063, 0.6622506939995727, 0.6357087065297982};
		double[] raw3k_step2 = {0.6323346796901159, 0.6111921439885145, 0.6160471880460118, 0.6189148586009574, 0.6722296427460032, 0.6166943393723369, 0.6403537191289669, 0.6779659558289333, 0.6468884602216265, 0.6530980979509875};
		
		double[] OurRaw2 = {0.964099168161956, 0.964099168161956, 0.962050100296422, 0.962886910702656, 0.966131834547496, 0.960139477822385, 0.96395962044697, 0.969372767565264, 0.965313739896153, 0.964871516992837};
		
		String[] classifierNames4 = {"step1_raw", "step1_raw200", "step1_raw300", "step1_raw400", "step1_raw500", "step1_raw750", "step1_raw1k", "step1_raw1.5k", "step1_raw2k", "step1_raw2.5k", "step1_raw3k", "step1_raw5k", "step1_raw7.5k", "step1_raw10k"};
		double[][] allFM4 = {step1raw, raw200_step1, raw300_step1, raw400_step1, raw500_step1, raw750_step1, raw1k_step1, raw1k5_step1, raw2k_step1, raw2k5_step1, raw3k_step1, raw5k_step1, raw7k5_step1, raw10k_step1};
//		double[] F14 = {0.833, 0.838, 0.838, 0.839, 0.840, 0.841, 0.796, 0.839, 0.840, 0.840, 0.840, 0.841, 0.839, 0.836};
//		double[] numInst4 = {numInst, numInst, numInst, numInst, numInst, numInst, numInst, numInst, numInst, numInst, numInst, numInst, numInst, numInst};
		
		StatisticsTests.compareResult(allFM4, classifierNames4, false);
//		StatisticsTests.compareResult2(F14, numInst4, classifierNames4, false);
		
		String[] classifierNames5 = {"step2_raw", "step2_raw200", "step2_raw300", "step2_raw400", "step2_raw500", "step2_raw750", "step2_raw1k", "step2_raw1.5k", "step2_raw2k", "step2_raw2.5k", "step2_raw3k"};
		double[][] allFM5 = {step2raw, raw200_step2, raw300_step2, raw400_step2, raw500_step2, raw750_step2, raw1k_step2, raw1k5_step2, raw2k_step2, raw2k5_step2, raw3k_step2};
//		double[] F15 = {0.661, 0.663, 0.662, 0.651, 0.647, 0.647, 0.631, 0.652, 0.653, 0.647, 0.663};
//		double[] numInst5 = {numInstSATD, numInstSATD, numInstSATD, numInstSATD, numInstSATD, numInstSATD, numInstSATD, numInstSATD, numInstSATD, numInstSATD, numInstSATD};
		
		StatisticsTests.compareResult(allFM5, classifierNames5, false);
//		StatisticsTests.compareResult2(F15, numInst5, classifierNames5, false);

		String[] classifierNames10 = {"OurRaw2", "OurRaw"};
		double[][] allFM10 = {OurRaw2, OurRaw};
//		double[] F110 = {0.965, 0.965};
//		double[] numInst10 = {numInst, numInst};
		
		StatisticsTests.compareResult(allFM10, classifierNames10, false);
//		StatisticsTests.compareResult2(F110, numInst10, classifierNames10, false);
		
		
		//RQ3 (step1)
		double[] SS1_750_step1 = {};
		double[] SS5_750_step1 = {};
		double[] SS10_750_step1 = {};
		//RQ3 (step2)
		double[] SS1_3k_step2 = {0.42172965532779927, 0.4229098117158324, 0.385667073176713, 0.4842428209488106, 0.5102737748248205, 0.49803526957846717, 0.42847183363543584, 0.4305603844664501, 0.22715932852168436, 0.38659078342772246};
		double[] SS5_3k_step2 = {0.5296327683615819, 0.522258646248083, 0.5371948428938312, 0.49375374150704526, 0.5521312249232948, 0.5190000824678133, 0.4151460567624963, 0.499590336336617, 0.5076709627469622, 0.5222176727990451};
		double[] SS10_3k_step2 = {0.610490502488166, 0.5556538238660542, 0.6188652371785599, 0.5279575515988072, 0.6657708947644991, 0.5611218784055465, 0.5754213795458973, 0.5870182652034406, 0.6393692074303118, 0.5980233270035591};
		double[] SM50_3k_step2 = {0.6744676834793113, 0.6935101551998578, 0.708199871877002, 0.6591586227863495, 0.7471625542394462, 0.6950908663121759, 0.7006401115749047, 0.6856753342033995, 0.7038061189066944, 0.685625208443485};
		double[] SM100_3k_step2 = {0.6889383801969345, 0.6884806329899787, 0.7100349507908085, 0.6862666389888598, 0.7507216708604524, 0.7147025288789849, 0.6975678494988918, 0.6934792897685275, 0.7363942295996562, 0.703997372003978};
		double[] SM150_3k_step2 = {0.6754973963196104, 0.6802898450802177, 0.704784382480866, 0.6926972213109917, 0.754473010439181, 0.7225162873339538, 0.7021352246210149, 0.6994259480612425, 0.7006013628713491, 0.6743519661967882};
		
		String[] classifierNames6 = {"step1_raw750", "SS1_750_step1", "SS5_750_step1", "SS10_750_step1"}; //all features
		double[][] allFM6 = {raw750_step1, SS1_750_step1, SS5_750_step1, SS10_750_step1};
//		double[] F16 = {0.841, 0.773, 0.758, 0.841};
//		double[] numInst6 = {numInst, numInst, numInst, numInst};
		
		StatisticsTests.compareResult(allFM6, classifierNames6, false);
//		StatisticsTests.compareResult2(F16, numInst6, classifierNames6, false);

		String[] classifierNames7 = {"step2_raw3k", "SS1_3k_step2", "SS5_3k_step2", "SS10_3k_step2", "SM50_3k_step2", "SM100_3k_step2", "SM150_3k_step2"};  //2k features
		double[][] allFM7 = {raw3k_step2, SS1_3k_step2, SS5_3k_step2, SS10_3k_step2, SM50_3k_step2, SM100_3k_step2, SM150_3k_step2};
//		double[] F17 = {0.663, 0.350, 0.541, 0.608, 0.692, 0.704, 0.701};
//		double[] numInst7 = {numInstSATD, numInstSATD, numInstSATD, numInstSATD, numInstSATD, numInstSATD, numInstSATD};
		
		StatisticsTests.compareResult(allFM7, classifierNames7, false);
//		StatisticsTests.compareResult2(F17, numInst7, classifierNames7, false);
		
		
		// final configuration
		double[] our = {0.965057001253074, 0.963462905914325, 0.963333744565366, 0.963474525114192, 0.968083539369915, 0.961229255402061, 0.965265693470821, 0.971157756251466, 0.966623254212384, 0.966554360384474};
		
		String[] classifierNames8 = {"Our", "1stStep", "2ndStep", "2NBM", "2LSVM", "2AB", "NLP", "PB", "OurRaw2"};
		double[][] allFM8 = {our, SS10_750_step1, SM100_3k_step2, twoNBM, twoLSVM, twoAB, NLP, PB, OurRaw2};
//		double[] F18 = {0.969, 0.841, 0.704, 0.949, 0.966, 0.959, 0.662, 0.098, 0.965};
//		double[] numInst8 = {numInst, numInst, numInstSATD, numInst, numInst, numInst, numInst, numInst, numInst};
		
		StatisticsTests.compareResult(allFM8, classifierNames8, false);
//		StatisticsTests.compareResult2(F18, numInst8, classifierNames8, false);
		
	}

}
