package twoStepsClassification;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import dataManager.DataHandler;
import dataManager.FeaturesHandler;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.bayes.NaiveBayesMultinomial;
import weka.classifiers.functions.LibSVM;
import weka.classifiers.functions.SMO;
import weka.classifiers.functions.supportVector.RBFKernel;
import weka.classifiers.meta.AdaBoostM1;
import weka.classifiers.meta.FilteredClassifier;
import weka.core.Instances;

public class TwoStepClassification {

	/*
	 * two steps classifier: 
	 * - first step: binary classification between SATD and WITHOUT_CLASSIFICATION. 
	 * - second step: multi-class classification between SATD classes: TEST,IMPLEMENTATION,DESIGN,DEFECT,DOCUMENTATION
	 */

	public static void main(String[] args) throws Exception {

		// upload original csv, data cleaning, split data into training and test set
		// (with set the seed of the random instance)
		String path = "./datasets/technical_debt_dataset.csv";

		Map<String, String> comments = DataHandler.loadFile(path);
		Instances instances = DataHandler.creatingArff(comments);

		// divide 70-30 with instances chooses at random
		Instances[] data = DataHandler.divideRandom(instances);
		System.out.println(instances.numInstances() + " " + data[0].numInstances() + " " + data[1].numInstances()); // data, training, test
		DataHandler.saveData(data[0], "training", data[1], "test");
		Instances training = data[0];
		Instances test = data[1];
		training.setClassIndex(training.numAttributes() - 1);
		test.setClassIndex(test.numAttributes() - 1);


		// transform data in binary-class
		Instances binTraining = FeaturesHandler.Binarization(training, true);
		Instances binTest = FeaturesHandler.Binarization(test, true);
		DataHandler.saveData(binTraining, "training_binary", binTest, "test_binary");
		binTraining.setClassIndex(binTraining.numAttributes() - 1);
		binTest.setClassIndex(binTest.numAttributes() - 1);

		
		// multi-class classification: remove records in WITHOUT_CLASSIFICATION class
		Instances multiTraining = FeaturesHandler.removeClass(training);
		Instances multiTest = FeaturesHandler.removeClass(test);
		DataHandler.saveData(multiTraining, "training_multi", multiTest, "test_multi");
		multiTraining.setClassIndex(multiTraining.numAttributes() - 1);
		multiTest.setClassIndex(multiTest.numAttributes() - 1);
		

		// classifiers to consider for classification task
//		NaiveBayesMultinomial nbm = new NaiveBayesMultinomial();

		// radial basis function: exp(-gamma*|u-v|^2)
		LibSVM libSVM = new LibSVM();
		libSVM.setSeed(8);
		libSVM.setGamma(0.0);
		libSVM.setCost(1.0);
		// default settings: c=1.0	kernel's gamma=0.01
		SMO smo = new SMO();
		smo.setRandomSeed(8);
		RBFKernel kernel = new RBFKernel();
		smo.setKernel(kernel);

//		AdaBoostM1 adaBoost = new AdaBoostM1();

		Classifier[] classificationModel = { smo }; // nbm, libSVM, smo, adaBoost
		String[] classifierName = { "SMO" }; //"NBM", "LibSVM", "SMO", "AdaBoost"

		// best1 is the best configuration for the 1st step (binary): SMO, no features selection, SpreadSubsample5.
		// best2 is the best configuration for the 2nd step (multi): SMO, 2k feature selection, SMOTE100.
		String preCombination = "best-combination";
		
		
		File tmpDir = new File("/output_twoSteps.csv");
		boolean exists = tmpDir.exists(); // true if the csv does not already exists
		

		Classifier classifier = classificationModel[0];

		// perform the grid search for classifiers that have parameters to be optimized.
		LibSVM lib = new LibSVM();
		SMO smoIn = new SMO();
		
		if (lib.getClass().isInstance(classifier)) { 
		 	classifier = (LibSVM) Classification.parametersOptimization(binTraining, classifier, classifierName[0], "binary"); 
		} else if (smoIn.getClass().isInstance(classifier)) { 
			classifier = (SMO) Classification.parametersOptimization(binTraining, classifier, classifierName[0], "binary"); 
		}
		
		// 10 fold CV 
		double[] BinaryfMeasure = Classification.tenFoldCV(binTraining, classifier, classifierName[0], "binary");
//		double[][][] BinaryCMs = Classification.extractConfusionMatrices(binTraining, classifier, "binary");
		System.out.println(Arrays.toString(BinaryfMeasure));
//		System.out.println(Arrays.deepToString(BinaryCMs));
		
		// evaluate model with test set 
		FilteredClassifier binaryClass = Classification.train(binTraining, classifier, "binary"); 
		Evaluation eval1 = new Evaluation(binTraining); 
		eval1.evaluateModel(binaryClass, binTest);
		System.out.println(eval1.toSummaryString());
		System.out.println(eval1.toClassDetailsString());
		 
		List<Object> classes = Collections.list(binTraining.classAttribute().enumerateValues());

		List<String> header = new ArrayList<>();
		header.add("combination");
		Classification.createHeader(header, classes);
		List<Object> results = new ArrayList<>();
		results.add(classifierName[0] + preCombination);
		Classification.createResults(results, eval1, classes, BinaryfMeasure);

		// multi-class classifier, 2nd step
		classifier = classificationModel[0];
		// perform the grid search for classifiers that have parameters to be optimized.
		if (lib.getClass().isInstance(classifier)) {
			classifier = (LibSVM) Classification.parametersOptimization(multiTraining, classifier,
					classifierName[0], "multi");
		} else if (smoIn.getClass().isInstance(classifier)) {
			classifier = (SMO) Classification.parametersOptimization(multiTraining, classifier, classifierName[0],
					"multi");
		}

		// 10 fold CV
		double[] MultifMeasure = Classification.tenFoldCV(multiTraining, classifier, classifierName[0], "multi");
		double[][][] MultiCMs = Classification.extractConfusionMatrices(multiTraining, classifier, "multi");
		
		System.out.println(Arrays.toString(MultifMeasure));
		System.out.println(Arrays.deepToString(MultiCMs));
		
		FilteredClassifier multiClass = Classification.train(multiTraining, classifier, "multi");
		Evaluation eval2 = new Evaluation(multiTraining);
		eval2.evaluateModel(multiClass, multiTest);
		System.out.println(eval2.toSummaryString());
		System.out.println(eval2.toClassDetailsString());

 		if (exists) {
			Classification.evaluatorFirst(eval2, multiTraining, header, results, MultifMeasure);
			exists = false;
		} else {
			Classification.evaluator(eval2, multiTraining, results, MultifMeasure);
		}

	
/*		/// calculate the mixed confusion matrix extracted from the test evaluation
		double[][] CM1 = eval1.confusionMatrix();
		double[][] CM2 = eval2.confusionMatrix();
		StatisticCalculator.createMixedFM(CM1, CM2);
		
		// calculate the mixed confusion matrices merging the two classification steps matrices for each CV round
		for (int i = 0; i < 10; i++) {
			
			StatisticCalculator.createMixedFM(BinaryCMs[i], MultiCMs[i]);
			
		}
*/		
		
		
/*		// save models
		weka.core.SerializationHelper.write("C:/Users/irene/eclipse-workspace/SATD/preTrainedModels/DebtHunterBinaryClassifier.model", binaryClass);
		weka.core.SerializationHelper.write("C:/Users/irene/eclipse-workspace/SATD/preTrainedModels/DebtHunterMultiClassifier.model", multiClass);
*/
	}

}