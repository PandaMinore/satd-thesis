# The Development of a Tool for Self-Admitted Technical Debt Detection

Università degli Studi di Milano Bicocca, Scuola di Scienze, Dipartimento di Informatica, Sistemistica e Comunicazione, Corso di Laurea Magistrale in Informatica

Thesis Overleaf link: https://www.overleaf.com/read/kkttrmdvkxtg

## How to read performaces in output .csv files (DebtHunter approch and baseline approaches perfromances):

Row name contains information about the classificator settings:
* The first word can be the model acronyms. the acronyms used are:
  * NB: naive bayes
  * NBM: naive bayes multinomial
  * RF: random forest
  * SMO: SMO (SVM)
* The 2nd word can be one of this **resampling** methods: Smote, Resample, nosampling (no resampling is performed).
* The 3rd word can be one of noReplacement, Replacement, none.
* The 4th word indicate the type of **tokenizer** is used. Tokenizer options: AlphabeticTokenizer, CharacterNGramTokenizer, NGramTokenizer, WordTokenizer(default)
* The 5th word indicate the **stopwords** list is used. Stopword options: Rainbow(default), RegExpFromFile, WordsFromFile, MultiStopWord, AbstractFileBasedStopwords, AbstractStopwords, Null.
* The 6th word indicate the type of **stemmer** is used. stemmer options: SnowballStemmer, LovinsStemmer, IteratedLovinStemmer
* The 7th word is the **number of words** to include in the dictionary for each class. I tried with 2000 or 1000.
* The 8th word is a number that represent the **percentage of sample size** (when resemple is performed). By default we have 100.

